# EasyShare-API

---

# Descrição

EasyShare é uma plataforma de angariação de fundos, funcionando como uma vaquinha virtual focado em causas sociais. Através dela é possível:

- Criar projetos para angariar fundos
- Doar para projetos

## Visual

Link para a prototipação das telas no Figma do [projeto](https://www.figma.com/file/yG8LgwTprJCPZCZMpIQSEI/EasyShare2?node-id=0%3A1).

## Instalação

- Instalar docker e doker compose
- Renomear o .env.example para .env
- Rodar yarn ou npm i para instalar dependências
- Rodar docker compose up

## Rorando a primeira vez

- Rodar o comando: doker compose up

## License

For open source projects, say how it is licensed.

## Status do projeto

Em andamento
