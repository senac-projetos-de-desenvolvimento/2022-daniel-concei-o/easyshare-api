import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { GraphQLModule } from "@nestjs/graphql";
import { TypeOrmModule } from "@nestjs/typeorm";
import { join } from "path";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UsersModule } from "./modules/users/users.module";
import { AdressesModule } from "./modules/adresses/adresses.module";
import { AuthModule } from "./modules/auth/auth.module";
import { ProjectsModule } from "./modules/projects/projects.module";
import { FilesModule } from "./modules/files/files.module";
import { FileController } from "./modules/files/controllers/file.controller";
import { PaymentGatewayModule } from './modules/payment-gateway/payment-gateway.module';
import { BanksModule } from './modules/banks/banks.module';
import { MonetaryTransactionsModule } from './modules/monetary-transactions/monetary-transactions.module';
import { PixPaymentKeysModule } from './modules/pix-payment-keys/pix-payment-keys.module';
import { FavoritesModule } from './modules/favorites/favorites.module';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), "src/schema.gql"),
      sortSchema: true,
      context: ({ req }) => ({ req }),
    }),
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(),
    UsersModule,
    AdressesModule,
    AuthModule,
    ProjectsModule,
    FilesModule,
    PaymentGatewayModule,
    BanksModule,
    MonetaryTransactionsModule,
    PixPaymentKeysModule,
    FavoritesModule,
  ],
  controllers: [AppController, FileController],
  providers: [AppService],
})
export class AppModule {}
