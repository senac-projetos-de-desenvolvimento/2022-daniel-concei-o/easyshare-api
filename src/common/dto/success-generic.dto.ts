import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class SuccessGenericDTO {
  @Field()
  success: boolean

  @Field()
  message: string
}