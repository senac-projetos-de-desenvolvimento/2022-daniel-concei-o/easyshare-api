export class BasicAuth {
    public static getGatewayToken(): string {
      const authorization = Buffer.from(
        process.env.PAGARME_API_KEY + ':' + process.env.PAGARME_PASSWORD
      ).toString('base64')
      return authorization
    }
  
    public static getHeader() {}
  }