import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as graphqlUploadExpress from "graphql-upload/graphqlUploadExpress.js";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(graphqlUploadExpress({ maxFiles: 10, maxFileSize: 10000000 }));

  await app.listen(parseInt(process.env.PORT) || 4000);
}
bootstrap();
