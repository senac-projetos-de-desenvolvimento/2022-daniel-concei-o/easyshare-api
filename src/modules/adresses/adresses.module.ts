import { Module } from "@nestjs/common";
import { Adress } from "./entities/adress.entity";
import {
  NestjsQueryGraphQLModule,
  PagingStrategies,
} from "@nestjs-query/query-graphql";
import { NestjsQueryTypeOrmModule } from "@nestjs-query/query-typeorm";
import { AdressDTO } from "./dto/adress.dto";
import { CreateAdressInput } from "./dto/create-adress.input";
import { UpdateAdressInput } from "./dto/update-adress.input";

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Adress])],
      resolvers: [
        {
          DTOClass: AdressDTO,
          EntityClass: Adress,
          CreateDTOClass: CreateAdressInput,
          UpdateDTOClass: UpdateAdressInput,
          enableTotalCount: true,
          pagingStrategy: PagingStrategies.OFFSET,
        },
      ],
    }),
  ],

  providers: [],
})
export class AdressesModule {}
