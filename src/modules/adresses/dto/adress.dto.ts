import {
  FilterableField,
  FilterableOffsetConnection,
} from "@nestjs-query/query-graphql";
import { ObjectType, Field, Int } from "@nestjs/graphql";
import { BaseDTO } from "src/modules/bases/dto/base.dto";
import { UserDTO } from "src/modules/users/dto/user.dto";

@ObjectType("Address")
@FilterableOffsetConnection("users", () => UserDTO, { nullable: true })
export class AdressDTO extends BaseDTO {
  @Field()
  id: string;

  @FilterableField()
  street: string;

  @FilterableField({ nullable: true })
  complement?: string;

  @FilterableField()
  district?: string;

  @FilterableField()
  CEP: string;

  @FilterableField()
  city: string;

  @FilterableField()
  state: string;

  @FilterableField({ nullable: true })
  number?: number;
}
