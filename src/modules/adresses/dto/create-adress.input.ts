import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class CreateAdressInput {
  @Field()
  street: string;

  @Field({ nullable: true })
  complement?: string;

  @Field()
  district?: string;

  @Field()
  CEP: string;

  @Field()
  city: string;

  @Field()
  state: string;

  @Field({ nullable: true })
  number: number;
}
