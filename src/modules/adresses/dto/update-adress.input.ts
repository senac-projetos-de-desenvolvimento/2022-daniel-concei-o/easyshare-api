import { CreateAdressInput } from "./create-adress.input";
import { InputType, Field, PartialType, ID } from "@nestjs/graphql";

@InputType()
export class UpdateAdressInput extends PartialType(CreateAdressInput) {
  @Field(() => ID, { nullable: true })
  id?: string;
}
