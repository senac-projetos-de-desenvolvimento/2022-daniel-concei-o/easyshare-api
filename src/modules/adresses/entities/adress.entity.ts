import { BaseEntity } from "src/modules/bases/entities/base.entity";
import { User } from "src/modules/users/entities/user.entity";
import { Column, Entity, OneToMany } from "typeorm";

@Entity()
export class Adress extends BaseEntity {
  @Column()
  street: string;

  @Column({ nullable: true })
  complement?: string;

  @Column()
  district?: string;

  @Column()
  CEP: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column({ nullable: true })
  number: number;

  @OneToMany(() => User, (users) => users.address, {
    nullable: true,
  })
  users: User[];
}
