import { Module } from '@nestjs/common';
import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Bank } from './entities/bank.entity';
import { BankDTO } from './dto/bank.dto';
import { CreateBankInput } from './dto/create-bank.input';
import { UpdateBankInput } from './dto/update-bank.input';

@Module({
  imports:[
    NestjsQueryGraphQLModule.forFeature({
      imports:[NestjsQueryTypeOrmModule.forFeature([Bank])],
      resolvers:[
        {
          DTOClass: BankDTO,
          EntityClass: Bank,
          CreateDTOClass: CreateBankInput,
          UpdateDTOClass:UpdateBankInput,
          enableTotalCount: true,
          pagingStrategy:PagingStrategies.OFFSET
        }
      ]
    })
  ],
  providers: []
})
export class BanksModule {}
