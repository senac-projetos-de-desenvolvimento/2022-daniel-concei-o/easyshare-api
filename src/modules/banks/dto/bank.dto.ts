import { ID, ObjectType } from '@nestjs/graphql'

import { FilterableField } from '@nestjs-query/query-graphql'

@ObjectType('Bank')
export class BankDTO {
  @FilterableField(() => ID)
  id: string

  @FilterableField()
  name: string

  @FilterableField()
  Cod: string
}
