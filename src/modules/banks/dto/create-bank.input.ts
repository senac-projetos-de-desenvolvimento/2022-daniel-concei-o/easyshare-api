import { InputType,  Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreateBankInput {
  @IsNotEmpty()
  @Field()
  name: string

  @IsNotEmpty()
  @Field()
  cod: string
}
