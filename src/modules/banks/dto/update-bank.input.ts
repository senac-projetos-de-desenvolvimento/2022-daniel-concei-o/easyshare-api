import { CreateBankInput } from './create-bank.input';
import { InputType, Field, Int, PartialType, ID } from '@nestjs/graphql';

@InputType()
export class UpdateBankInput extends PartialType(CreateBankInput) {
  @Field(() => ID)
  id: string;
}
