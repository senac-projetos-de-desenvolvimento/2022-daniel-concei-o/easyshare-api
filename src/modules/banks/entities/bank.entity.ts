import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Bank{
  @PrimaryGeneratedColumn()
  id: string

  @Column()
  name: string

  @Column()
  Cod: string
}
