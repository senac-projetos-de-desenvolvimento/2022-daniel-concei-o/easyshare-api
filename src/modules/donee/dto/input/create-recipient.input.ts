import { Field, InputType } from '@nestjs/graphql'

@InputType('CreateRecipientInput')
export class CreateRecipientInput {
  @Field()
  name: string

  @Field()
  email: string

  @Field()
  description: string

  @Field()
  document: string

  @Field()
  type: string

  @Field()
  holder_name: string

  @Field()
  holder_type: string

  @Field()
  holder_document: string

  @Field()
  bank: string

  @Field()
  branch_number: string

  @Field()
  branch_check_digit: string

  @Field()
  account_number: string

  @Field()
  account_check_digit: string
}