import { ObjectType } from '@nestjs/graphql'


import { FilterableField } from '@nestjs-query/query-graphql'
import { BaseDTO } from 'src/modules/bases/dto/base.dto'

@ObjectType('Recipient')
export class RecipientDTO extends BaseDTO {
  @FilterableField()
  name?: string

  @FilterableField()
  email?: string

  @FilterableField()
  recipientId?: string

  @FilterableField()
  id?: string

  @FilterableField()
  holder_name?: string

  @FilterableField()
  holder_type?: string

  @FilterableField()
  holder_document?: string

  @FilterableField()
  bank?: string

  @FilterableField()
  branch_number?: string

  @FilterableField({nullable:true})
  branch_check_digit?: string

  @FilterableField()
  account_number?: string

  @FilterableField({nullable:true})
  account_check_digit?: string

  @FilterableField()
  type?: string

  @FilterableField()
  status?: string
}
