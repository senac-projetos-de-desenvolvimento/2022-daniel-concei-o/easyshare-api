import { InputType } from "@nestjs/graphql";

@InputType()
export class CreateFavoriteInput {
  userId:string

  projectId:string
}
