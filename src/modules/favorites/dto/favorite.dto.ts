import { BaseDTO } from "@easyshare/modules/bases/dto/base.dto";
import { ProjectDTO } from "@easyshare/modules/projects/dto/project.dto";
import { UserDTO } from "@easyshare/modules/users/dto/user.dto";
import {
  FilterableRelation,
} from "@nestjs-query/query-graphql";
import { ObjectType } from "@nestjs/graphql";

@ObjectType("Favorite")
@FilterableRelation("user", () => UserDTO, { nullable: true })
@FilterableRelation("project", () => ProjectDTO, { nullable: true })
export class FavoriteDTO extends BaseDTO {}
