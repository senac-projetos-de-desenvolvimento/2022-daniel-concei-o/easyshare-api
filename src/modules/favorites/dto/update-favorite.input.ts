import { CreateFavoriteInput } from './create-favorite.input';
import { InputType, Field, PartialType, ID } from '@nestjs/graphql';

@InputType()
export class UpdateFavoriteInput extends PartialType(CreateFavoriteInput) {
  @Field(() => ID, {nullable: true})
  id: string;
}
