import { BaseEntity } from "@easyshare/modules/bases/entities/base.entity";
import { Project } from "@easyshare/modules/projects/entities/project.entity";
import { User } from "@easyshare/modules/users/entities/user.entity";
import { Column, Entity, ManyToOne } from "typeorm";

@Entity()
export class Favorite extends BaseEntity {
   @ManyToOne(() => User, (user) => user.favorites, {
     nullable: true,
     cascade: true,
   })
   user: User;

   @Column({nullable:true})
   userId: string

   @ManyToOne(() => Project, (project) => project.favorites, {
     nullable: true,
     cascade: true,
   })
   project: Project;

   @Column({nullable:true})
   projectId: string
}
