import { Module } from "@nestjs/common";
import { FavoritesService } from "./favorites.service";
import { FavoritesResolver } from "./favorites.resolver";
import {
  NestjsQueryGraphQLModule,
  PagingStrategies,
} from "@nestjs-query/query-graphql";
import { NestjsQueryTypeOrmModule } from "@nestjs-query/query-typeorm";
import { Favorite } from "./entities/favorite.entity";
import { FavoriteDTO } from "./dto/favorite.dto";
import { CreateFavoriteInput } from "./dto/create-favorite.input";
import { UpdateFavoriteInput } from "./dto/update-favorite.input";
import { User } from "../users/entities/user.entity";
import { UsersModule } from "../users/users.module";

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryTypeOrmModule.forFeature([Favorite, User]),
        UsersModule,
      ],
      resolvers: [
        {
          DTOClass: FavoriteDTO,
          EntityClass: Favorite,
          CreateDTOClass: CreateFavoriteInput,
          UpdateDTOClass: UpdateFavoriteInput,
          enableTotalCount: true,
          pagingStrategy: PagingStrategies.OFFSET,
        },
      ],
    }),
  ],
  providers: [FavoritesResolver, FavoritesService],
})
export class FavoritesModule {}
