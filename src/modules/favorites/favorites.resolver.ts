import { Resolver, Mutation, Args, } from "@nestjs/graphql";
import { FavoritesService } from "./favorites.service";
import { Favorite } from "./entities/favorite.entity";
import { GqlAuthGuard } from "../auth/auth.guard";
import { UseGuards } from "@nestjs/common";
import { User } from "../users/entities/user.entity";
import { CurrentUser } from "../decorator/current-user";
import { SuccessGenericDTO } from "@easyshare/common/dto/success-generic.dto";

@Resolver(() => Favorite)
export class FavoritesResolver {
  constructor(private readonly favoritesService: FavoritesService) {}

  @Mutation(() => SuccessGenericDTO)
  @UseGuards(GqlAuthGuard)
  public async createOrDeleteOneUserLike(
    @CurrentUser() currentUser: User,
    @Args("ProjectId") projectId: string
  ): Promise<SuccessGenericDTO> {
    return await this.favoritesService.createOrDeleteLike(
      currentUser,
      projectId
    );

  }
}
