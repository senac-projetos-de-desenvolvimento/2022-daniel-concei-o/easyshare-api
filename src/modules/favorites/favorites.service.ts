import { Injectable } from "@nestjs/common";
import { User } from "../users/entities/user.entity";
import { Favorite } from "./entities/favorite.entity";
import { TypeOrmQueryService } from "@nestjs-query/query-typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

@Injectable()
export class FavoritesService extends TypeOrmQueryService<Favorite> {
  constructor(
    @InjectRepository(Favorite)
    public favoriteRepository: Repository<Favorite>
  ) {
    super(favoriteRepository, { useSoftDelete: false });
  }

  async createOrDeleteLike(currentUser: User, projectId: string) {
    const favoriteData: Favorite = await this.favoriteRepository.findOne({
      where: [{ userId: currentUser.id, projectId: projectId }],
    });

    if (!favoriteData) {
      await this.createOne({
        userId: currentUser.id,
        projectId: projectId,
      });

      return {
        success: true,
        message: "Favorite created",
      };
    } else {
      await this.favoriteRepository.remove(favoriteData);
      return { success: true, message: "Favorite deleted" };
    }
  }
}
