import { InputType, Field } from "@nestjs/graphql";
import { IsOptional } from "class-validator";

@InputType()
export class CreateFileInput {
  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  path?: string;

  @Field({ nullable: true })
  readonly id?: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly fileName?: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly filePath?: string;
}
