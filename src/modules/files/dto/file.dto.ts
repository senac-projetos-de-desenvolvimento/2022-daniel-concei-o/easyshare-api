import {
  FilterableField,
  FilterableRelation,
} from "@nestjs-query/query-graphql";
import { Field, ObjectType } from "@nestjs/graphql";
import { IsOptional } from "class-validator";
import { BaseDTO } from "src/modules/bases/dto/base.dto";
import { ProjectDTO } from "src/modules/projects/dto/project.dto";

@ObjectType("File")
@FilterableRelation("project", () => ProjectDTO, { nullable: true })
export class FileDTO extends BaseDTO {
  @FilterableField({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  path?: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly fileName?: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly filePath?: string;
}
