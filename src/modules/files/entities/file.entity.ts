import { BaseEntity } from "src/modules/bases/entities/base.entity";
import { Project } from "src/modules/projects/entities/project.entity";
import { Column, Entity, ManyToOne } from "typeorm";

@Entity()
export class File extends BaseEntity {
  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  path?: string;

  @Column({ nullable: true })
  fileName: string;

  @ManyToOne(() => Project, (project) => project.images, {
    nullable: true,
    onDelete: "CASCADE",
  })
  project: Project;

  @Column({ name: "file_size", nullable: true })
  fileSize: string;

  @Column({ name: "file_format", nullable: true })
  fileFormat: string;

  @Column({ name: "original_file_name", nullable: true })
  originalFileName: string;

  @Column({ nullable: true, default: false })
  isPrivate: boolean;
}
