import { Module } from "@nestjs/common";

import { FilesResolver } from "./files.resolver";
import {
  NestjsQueryGraphQLModule,
  PagingStrategies,
} from "@nestjs-query/query-graphql";
import { NestjsQueryTypeOrmModule } from "@nestjs-query/query-typeorm";
import { File } from "./entities/file.entity";
import { FileDTO } from "./dto/file.dto";
import { CreateFileInput } from "./dto/create-file.input";
import { UpdateFileInput } from "./dto/update-file.input";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "@nestjs/config";
import { FileController } from "./controllers/file.controller";
import { FilesService } from "./files.service";

@Module({
  controllers: [FileController],
  imports: [
    TypeOrmModule.forFeature([File]),
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([File])],
      resolvers: [
        {
          DTOClass: FileDTO,
          EntityClass: File,
          CreateDTOClass: CreateFileInput,
          UpdateDTOClass: UpdateFileInput,
          enableTotalCount: true,
          pagingStrategy: PagingStrategies.OFFSET,
        },
      ],
    }),
    ConfigModule,
  ],
  exports: [FilesService],
  providers: [FilesResolver, FilesService],
})
export class FilesModule {}
