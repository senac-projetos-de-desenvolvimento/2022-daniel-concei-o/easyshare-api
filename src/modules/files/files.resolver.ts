import {
  Resolver,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from "@nestjs/graphql";

import * as GraphQLUpload from "graphql-upload/GraphQLUpload.js";
import FileUpload from "graphql-upload/Upload.js";

import { File } from "./entities/file.entity";
import { ConfigService } from "@nestjs/config";
import { FileDTO } from "./dto/file.dto";
import { FilesService } from "./files.service";

@Resolver(() => FileDTO)
export class FilesResolver {
  constructor(
    private filesService: FilesService,
    private configService: ConfigService
  ) {}

  @Mutation(() => FileDTO)
  public async addProfilePicture(
    @Args({ name: "file", type: () => GraphQLUpload })
    file: FileUpload
  ): Promise<File> {
    const data = await this.filesService.uploadFile({
      uploadFile: file,
    });
    return data;
  }

  @ResolveField()
  filePath(@Parent() file: FileDTO): string {
    return `${this.configService.get("SAVE_FILES_URL")}${file.id}`;
  }
}
