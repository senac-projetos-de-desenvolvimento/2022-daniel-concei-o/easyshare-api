import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";

import { existsSync, mkdirSync, unlinkSync } from "fs";
import { FileUpload } from "graphql-upload";
import * as sharp from "sharp";
import { Repository } from "typeorm";
import { File } from "./entities/file.entity";
@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(File)
    public fileRepository: Repository<File>
  ) {}

  UPLOAD_DIR = "./uploads";

  async getFilePathById(id: string): Promise<string> {
    const file = await this.fileRepository.findOne(id);
    const repo = this.UPLOAD_DIR;

    return `${repo}/${file.fileName}`;
  }

  async isFilePrivateById(id: string): Promise<boolean> {
    const file = await this.fileRepository.findOne(id);
    return file.isPrivate;
  }

  private async createFileWithSharp(input: {
    uploadedFile: FileUpload;
    fileName: string;
  }) {
    const { uploadedFile, fileName } = input;
    const buffer = await this.readStreamToBuffer(uploadedFile);
    const file = sharp(buffer);
    const metadata = await file.metadata();

    const repo = this.UPLOAD_DIR;

    if (!existsSync(repo)) {
      mkdirSync(repo, { recursive: true });
    }

    const filePath = `${repo}/${fileName}.${metadata.format}`;

    const writtedFile = await file.toFile(filePath);

    return {
      format: writtedFile.format,
      size: writtedFile.size.toString(),
    };
  }

  private async createFileLocally(input: {
    id;
    uploadedFile: FileUpload;
  }): Promise<sharp.OutputInfo> {
    const { id, uploadedFile } = input;
    const result = await this.createFileWithSharp({
      uploadedFile,
      fileName: id,
    });

    return {
      format: result.format,
      size: result.size,
    };
  }

  private readStreamToBuffer(fileUpload: FileUpload) {
    return new Promise(async (resolve) => {
      const buffers = [];
      fileUpload
        .createReadStream()
        .on("data", (data) => {
          buffers.push(data);
        })
        .on("end", () => {
          resolve(Buffer.concat(buffers));
        });
    });
  }

  getRelativeUploadDir(): string {
    return this.UPLOAD_DIR.replace("./static", "");
  }

  private async saveFileIntoDatabase(input: {
    uploadedFile: FileUpload;
  }): Promise<File> {
    const { uploadedFile } = input;
    const file: File = new File();
    file.fileName = uploadedFile.filename;
    file.originalFileName = uploadedFile.filename;

    return await this.fileRepository.save(file);
  }

  private async updateSavedFileWithSomeInfo(input: {
    created: File;
    infos: any;
  }): Promise<File> {
    const { created, infos } = input;
    const { id } = created;
    created.fileName = `${id}.${infos.format}`;
    created.fileSize = infos.size;
    created.fileFormat = infos.format;
    created.originalFileName = infos.uploadFile.filename;

    return await this.fileRepository.save(created);
  }

  async uploadFile(input: { uploadFile: FileUpload }): Promise<File> {
    const { uploadFile } = input;
    const created = await this.saveFileIntoDatabase({
      uploadedFile: uploadFile,
    });

    const { format, size } = await this.createFileLocally({
      id: created.id,
      uploadedFile: uploadFile,
    });

    return await this.updateSavedFileWithSomeInfo({
      created,
      infos: { format, size, uploadFile },
    });
  }

  async deleteFile(id: string): Promise<boolean> {
    const res = await this.fileRepository.findOne(id);

    const { fileName } = res;

    const repo = this.UPLOAD_DIR;

    unlinkSync(`${repo}/${fileName}`);

    await this.fileRepository.delete(id);

    return true;
  }
}
