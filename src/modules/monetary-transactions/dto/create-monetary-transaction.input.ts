import { UpdateUserInput } from '@easyshare/modules/users/dto/update-user.input'
import { Field, InputType } from '@nestjs/graphql'

import { IsEnum } from 'class-validator'
import { TransactionPaymentMethodEnum } from '../enums/monetary-transction-type.enum'
import { CreateCreditCardInput } from '@easyshare/modules/payment-gateway/dto/create-credit-card.input'

@InputType('CreateMonetaryTransactionInput')
export class CreateMonetaryTransactionInput {
  @Field()
  cardId?: string

  @Field(() => UpdateUserInput)
  user?: UpdateUserInput

  @Field(() => TransactionPaymentMethodEnum)
  @IsEnum(TransactionPaymentMethodEnum)
  methodPayment: TransactionPaymentMethodEnum

  @Field(() => CreateCreditCardInput)
  card?: CreateCreditCardInput

  @Field()
  donationAmount?: number

  @Field()
  installments?: number
}
