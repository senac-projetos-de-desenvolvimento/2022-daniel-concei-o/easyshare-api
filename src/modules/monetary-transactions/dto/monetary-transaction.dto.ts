import { ObjectType } from "@nestjs/graphql";
import {
  TransactionPaymentMethodEnum,
  TransactionStatusEnum,
} from "../enums/monetary-transction-type.enum";
import { BaseDTO } from "@easyshare/modules/bases/dto/base.dto";
import { FilterableField } from "@nestjs-query/query-graphql";

@ObjectType("MonetaryTransaction")
export class MonetaryTransactionDTO extends BaseDTO {
  @FilterableField(() => TransactionPaymentMethodEnum)
  methodPayment: TransactionPaymentMethodEnum;

  @FilterableField(() => TransactionStatusEnum)
  status: TransactionStatusEnum;

  @FilterableField({ nullable: true })
  qrCodeString: string;

  @FilterableField({ nullable: true })
  qrCodeUrlImage: string;

  @FilterableField({ nullable: true })
  installments: number;

  @FilterableField({ nullable: true })
  orderId: string;

  @FilterableField({ nullable: true })
  transId: string;

  @FilterableField({ nullable: true })
  boletoUrl?: string

  @FilterableField({ nullable: true })
  boletoPdfLink?: string

  @FilterableField({ nullable: true })
  boletoCode: string

  @FilterableField({ nullable: true })
  boletoBarCode: string
}
