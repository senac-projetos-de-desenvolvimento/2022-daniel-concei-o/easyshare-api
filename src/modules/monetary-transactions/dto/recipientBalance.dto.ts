import { Field, ObjectType } from "@nestjs/graphql";

import { BaseDTO } from "@easyshare/modules/bases/dto/base.dto";
import { FilterableField } from "@nestjs-query/query-graphql";

@ObjectType("RecipientData")
export class RecipientDataDTO {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field()
  email: string;

  @Field()
  document: string;

  @Field()
  type: string;

  @Field()
  payment_mode: string;
  
  @Field()
  status: string;
}


@ObjectType("RecipienteBalance")
export class RecipienteBalanceDTO extends BaseDTO {
  @FilterableField()
  currency?: string;

  @FilterableField()
  available_amount?: string;

  @FilterableField()
  waiting_funds_amount?: string;

  @Field(() => RecipientDataDTO)
  recipient?: RecipientDataDTO;
}

