import { CreateMonetaryTransactionInput } from './create-monetary-transaction.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateMonetaryTransactionInput extends PartialType(CreateMonetaryTransactionInput) {
  @Field(() => Int)
  id: number;
}
