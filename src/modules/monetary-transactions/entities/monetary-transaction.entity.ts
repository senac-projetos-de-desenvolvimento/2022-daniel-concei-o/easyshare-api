import { BaseEntity } from '@easyshare/modules/bases/entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { TransactionPaymentMethodEnum, TransactionStatusEnum } from '../enums/monetary-transction-type.enum';
import { User } from '@easyshare/modules/users/entities/user.entity';

@Entity()
export class MonetaryTransaction extends BaseEntity{
  @ManyToOne(() => User, user => user.transactions)
  user: User

  @Column({ nullable: true })
  totalAmount: number

  @Column('int', { nullable: true })
  installments: number

  @Column({ nullable: true })
  orderId: string

  @Column({ nullable: true })
  transId: string

  @Column({ nullable: true })
  qrCodeString: string

  @Column({ nullable: true })
  qrCodeUrlImage: string

  @Column({
    nullable: false,
    type: 'enum',
    enum: TransactionStatusEnum,
    default: TransactionStatusEnum.PENDENTE
  })
  status: TransactionStatusEnum

  @Column({
    nullable: false,
    type: 'enum',
    enum: TransactionPaymentMethodEnum
  })
  methodPayment: TransactionPaymentMethodEnum

  @Column({nullable: true})
  boletoUrl?: string

  @Column({nullable: true})
  boletoPdfLink?: string

  @Column({nullable: true})
  boletoCode: string

  @Column({nullable: true})
  boletoBarCode?: string
}
