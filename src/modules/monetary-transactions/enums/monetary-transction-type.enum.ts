import { registerEnumType } from '@nestjs/graphql'

export enum TransactionStatusEnum {
  PAGO = 'paid',
  PENDENTE = 'pending',
  CANCELADO = 'failed'
}

registerEnumType(TransactionStatusEnum, {
  name: 'TransactionStatusEnum'
})

export enum TransactionPaymentMethodEnum {
  BOLETO = 'boleto',  
  PIX = 'pix',
  CREDITO = 'credit_card'
}

registerEnumType(TransactionPaymentMethodEnum, {
  name: 'TransactionPaymentMethodEnum'
})
