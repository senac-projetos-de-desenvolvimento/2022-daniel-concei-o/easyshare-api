import { Module } from '@nestjs/common';
import { MonetaryTransactionsService } from './monetary-transactions.service';
import { MonetaryTransactionsResolver } from './monetary-transactions.resolver';
import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { MonetaryTransaction } from './entities/monetary-transaction.entity';
import { MonetaryTransactionDTO } from './dto/monetary-transaction.dto';
import { CreateMonetaryTransactionInput } from './dto/create-monetary-transaction.input';
import { UpdateMonetaryTransactionInput } from './dto/update-monetary-transaction.input';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryTypeOrmModule.forFeature([MonetaryTransaction]),
      ],
      resolvers: [
        {
          EntityClass: MonetaryTransaction,
          DTOClass: MonetaryTransactionDTO,
          CreateDTOClass: CreateMonetaryTransactionInput,
          UpdateDTOClass: UpdateMonetaryTransactionInput,
          pagingStrategy: PagingStrategies.OFFSET,
          create: { disabled: true },
          update: { disabled: true },
          delete: { disabled: true },
          enableAggregate: true,
          enableTotalCount: true,
          enableSubscriptions: true,
          
        }
      ]
    })
  ],
  providers: [MonetaryTransactionsResolver, MonetaryTransactionsService]
})
export class MonetaryTransactionsModule {}
