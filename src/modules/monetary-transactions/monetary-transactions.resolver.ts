import { Resolver, Mutation, Query, Args } from "@nestjs/graphql";
import { MonetaryTransactionsService } from "./monetary-transactions.service";
import { MonetaryTransaction } from "./entities/monetary-transaction.entity";
import { CreateMonetaryTransactionInput } from "./dto/create-monetary-transaction.input";
import { MonetaryTransactionDTO } from "./dto/monetary-transaction.dto";
import { User } from "../users/entities/user.entity";
import { CurrentUser } from "../decorator/current-user";
import { UseGuards } from "@nestjs/common";
import { GqlAuthGuard } from "../auth/auth.guard";
import { RecipienteBalanceDTO } from "./dto/recipientBalance.dto";

@Resolver(() => MonetaryTransaction)
export class MonetaryTransactionsResolver {
  constructor(
    private readonly monetaryTransactionsService: MonetaryTransactionsService
  ) {}

  @Mutation(() => MonetaryTransactionDTO)
  @UseGuards(GqlAuthGuard)
  public async createOneTransactionGateway(
    @CurrentUser() currentUser: User,
    @Args("data") data: CreateMonetaryTransactionInput,
    @Args("ProjectId") projectId: string
  ): Promise<MonetaryTransaction> {
    const response =
      await this.monetaryTransactionsService.createGatewayForMonetaryTransaction(
        currentUser,
        data,
        projectId
      );

    return response;
  }

  @Query(() => RecipienteBalanceDTO)
  @UseGuards(GqlAuthGuard)
  public async getRecipientBalance(
    @CurrentUser() currentUser: User
  ): Promise<RecipienteBalanceDTO> {
    const response = await this.monetaryTransactionsService.recipientBalance(
      currentUser
    );

    return response;
  }
}
