import { HttpException, Injectable } from "@nestjs/common";
import { CreateMonetaryTransactionInput } from "./dto/create-monetary-transaction.input";
import { User } from "../users/entities/user.entity";
import axios from "axios";
import { BasicAuth } from "@easyshare/common/utils/basic-auth.util";
import {
  DonationTypeInput,
  GatewayPagarmeWebhookInput,
} from "../payment-gateway/dto/create-payment-gateway.input";
import {
  TransactionPaymentMethodEnum,
  TransactionStatusEnum,
} from "./enums/monetary-transction-type.enum";
import { MonetaryTransaction } from "./entities/monetary-transaction.entity";
import { getRepository } from "typeorm";
import { Project } from "../projects/entities/project.entity";
import { add } from "date-fns";

@Injectable()
export class MonetaryTransactionsService {
  async createGatewayForMonetaryTransaction(
    user: User,
    data: CreateMonetaryTransactionInput,
    projectId: string
  ) {
    const project = await this.getProject(projectId);

    let response = null;
    const amount = data.donationAmount; //centavos

    const gatewayToken = BasicAuth.getGatewayToken();

    const items = [];
    const split = [];

    const item = {
      amount: amount,
      description: "doacao",
      quantity: 1,
      code: project.id,
    };
    items.push(item);

    const splitItem = {
      options: {
        charge_processing_fee: true,
        charge_remainder_fee: true,
        liable: true,
      },
      //usuário que vai receber o valor do split
      recipient_id: project.user.recipientId, //user.recipientId,

      type: "percentage",
      //valor destinado ao recebedor = 90%
      amount: 100,
    };
    split.push(splitItem);

    const Pix = {
      expires_in: 3600, //1h
    };

    const today = new Date();
    const dueDate = add(today, { days: 3 });

    const Boleto = {
      bank: "001", //banco do brasil
      instructions: "Pagar até o vencimento",
      due_at: dueDate,
      document_number: "123",
      type: "DM",
    };

    const orderType: DonationTypeInput = {
      //id do usuário que esta fazendo a doação
      customer_id: user.donatorId,
      payments: [
        {
          payment_method: data.methodPayment,
        },
      ],
    };

    orderType.items = items;
    orderType.payments[0].split = split;

    if (
      orderType.payments[0].payment_method ==
      TransactionPaymentMethodEnum.CREDITO
    ) {
      //já possui cartão de crédito cadastrado
      if (data.cardId) {
        const creditCard = {
          installments: data.installments,
          card_id: data.cardId,
        };
        orderType.payments[0].credit_card = creditCard;
      } else {
        const creditCard = {
          installments: data.installments,
          card: {
            billing_address: {
              line_1: data.card.line_1,
              zip_code: data.card.CEP,
              line_2: data.card.line_2,
              city: data.card.city, //data.card.address.cityId,
              state: data.card.stateInitials,
              country: "BR",
            },
            number: data.card.cardNumber,
            exp_month: +data.card.exp_month,
            exp_year: +data.card.exp_year,
            cvv: data.card.securityCode,
            holder_name: data.card.holder_name,
          },
        };
        orderType.payments[0].credit_card = creditCard;
      }
    }

    if (
      orderType.payments[0].payment_method == TransactionPaymentMethodEnum.PIX
    ) {
      orderType.payments[0].Pix = Pix;
    }

    if (
      orderType.payments[0].payment_method ==
      TransactionPaymentMethodEnum.BOLETO
    ) {
      orderType.payments[0].boleto = Boleto;
    }

    const myJSON = JSON.stringify(orderType);
    console.log("JSON: ", myJSON);

    await axios
      .post(`${process.env.PAYMENT_GATEWAY_URL}/orders`, myJSON, {
        headers: {
          Accept: "application/json",
          "content-type": "application/json",
          Authorization: `Basic ${gatewayToken}`,
        },
      })
      .then(async (res) => {
        response = {
          order_id: res.data.id,
          order_data: res.data,
          order_items: res.data.items,
          order_charge: res.data.charges,
          order_last_transaction: res.data.charges[0].last_transaction,
          req_status: res.data.charges[0].status,
          order_qr_code: res.data.charges[0].last_transaction.qr_code,
          order_qr_code_url: res.data.charges[0].last_transaction.qr_code_url,
          order_error:
            res.data.charges[0].last_transaction?.gateway_response?.errors,
        };
        console.log("Response: ", response);
      })
      .catch((err) => {
        const errors = err?.response?.data?.errors;
        const error = Object.values(errors)?.[0]?.[0];
        console.log("error: ", err);
        throw new HttpException(
          "GATEWAY_ERROR_WHEN_CREATING_TRANSACTION - " + error,
          err?.response?.status
        );
      });

    const dataOder: MonetaryTransaction = data as any;
    dataOder.totalAmount = +amount;
    dataOder.installments = response.order_last_transaction.installments;
    dataOder.transId = response.order_last_transaction.id;
    dataOder.orderId = response.order_id;
    dataOder.qrCodeString = response.order_qr_code;
    dataOder.qrCodeUrlImage = response.order_qr_code_url;
    dataOder.status = response.req_status;
    dataOder.user = user;
    dataOder.boletoUrl = response.order_last_transaction.url;
    dataOder.boletoPdfLink = response.order_last_transaction.pdf;
    dataOder.boletoCode = response.order_last_transaction.line;
    dataOder.boletoBarCode = response.order_last_transaction.barcode;

    console.log("dataOder ", dataOder);

    const transactionCreated = await getRepository(MonetaryTransaction).create(
      dataOder
    );

    await getRepository(MonetaryTransaction).save(transactionCreated);

    return transactionCreated;
  }

  async getProject(project: string): Promise<Project> {
    const projectData = await getRepository(Project).findOne({
      where: [{ id: project }],
    });

    return projectData;
  }

  async orderStatusChanged(data: GatewayPagarmeWebhookInput) {
    console.log("weebhook: ", GatewayPagarmeWebhookInput)
    switch (data?.type) {
      case "order.paid":
        await this.getTransactionByOrderIdAndPaid(data.data.id);
    }
  }

  async getTransactionByOrderIdAndPaid(orderId: string) {
    const transaction = await getRepository(MonetaryTransaction).findOne({
      where: [{ orderId: orderId }],
      relations: ["user"],
    });

    transaction.status = TransactionStatusEnum.PAGO;
    await getRepository(MonetaryTransaction).save(transaction);

    return transaction;
  }

  async recipientBalance(user: User) {
    const gatewayToken = BasicAuth.getGatewayToken();
    const recipient_id = user.recipientId;

    const axiosData = await axios.get(
      `${process.env.PAYMENT_GATEWAY_URL}/recipients/${recipient_id}/balance`,
      {
        headers: {
          Accept: "application/json",
          "content-type": "application/json",
          Authorization: `Basic ${gatewayToken}`,
        },
      }
    );

    const response = await axiosData.data

    console.log(response)

    return response
  }
}
