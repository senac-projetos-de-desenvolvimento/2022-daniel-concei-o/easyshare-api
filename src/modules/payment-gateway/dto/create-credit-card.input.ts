import { Field, InputType } from "@nestjs/graphql";

@InputType('CreateCreditCardInput')
export class CreateCreditCardInput{
    @Field()
    cardNumber: string

    @Field()
    holder_name: string

    @Field()
    CPF: string

    @Field()
    exp_month: string

    @Field()
    exp_year: string

    @Field()
    securityCode: string

    @Field()
    brand?: string
  
    @Field()
    label?: string
  
    @Field()
    line_1: string
  
    @Field()
    line_2: string
  
    @Field()
    CEP: string
  
    @Field()
    city: string
  
    @Field()
    country?: string
  
    @Field()
    stateInitials: string
}