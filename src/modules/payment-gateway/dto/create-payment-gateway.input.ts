import { InputType } from "@nestjs/graphql";


@InputType("DonatorDataInput")
export class DonatorDataInput {
  name: string;
  email?: string;
  code?: string;
  document?: string;
  document_type?: string;
  type?: string;
  birthDate: string;
  phones?: {
    mobile_phone: {
      country_code: string;
      area_code: string;
      number: string;
    };
  };
  address?: {
    country: string;
    state: string;
    city: string;
    zip_code: string;
    line_1: string;
    line_2: string;
  };
}

@InputType("DonationTypeInput")
export class DonationTypeInput {
  customer_id: string;
  items?: {
    amount: number;
    description: string;
    quantity: number;
    code: string;
  }[];

  payments?: [
    {
      split?: {
        options: {
          charge_processing_fee: boolean;
          charge_remainder_fee: boolean;
          liable: boolean;
        };
        recipient_id: string;
        type: string;
        amount: string;
      }[];

      credit_card?: {
        installments?: number;
        card_id?: string;
        card?: {
          billing_address: {
            line_1: string;
            zip_code: string;
            line_2: string;
            city: string;
            state: string;
            country: string;
          };
          number: string;
          exp_month: number;
          exp_year: number;
          cvv: string;
          holder_name: string;
        };
      };

      Pix?: {
        expires_in: number;
      };

      boleto?: {
        instructions: string;
        due_at: Date;
        document_number: string;
        type: string;
        bank: string;
      };

      payment_method: string;

      
    }
  ];

  customer?: {
    name: string;
    email: string;
    document_type: string;
    document: string; //cpf válido
    type: string;
    address: {
      line_1: string;
      line_2?: string;
      zip_code: string;
      city: string;
      state: string;
      country: string;
    };
  };
  shipping?: {
    amount: number;
    description: string;
    recipient_name: string;
    recipient_phone: string;
    address: {
      line_1: string;
      zip_code: string;
      city: string;
      state: string;
      country: string;
    };
  };
}

@InputType("OptionsSplit")
export class OptionsSplit {
  charge_processing_fee: boolean;
  charge_remainder_fee: boolean;
  liable: boolean;
}

@InputType("SplitInput")
export class SplitInput {
  options: OptionsSplit;
  recipient_id: string;
  type: string;
  amount: string;
}

@InputType("Pix")
export class Pix {
  expires_in: number;
}

export class GatewayPagarmeWebhookInput {
  id: string;

  account: GatewayWebhookAccount;

  type: string;

  created_at: Date;

  data: OrderCreatedType;
}

class GatewayWebhookAccount {
  id: string;

  name: string;
}

export class OrderCreatedType {
  id: string;
}
