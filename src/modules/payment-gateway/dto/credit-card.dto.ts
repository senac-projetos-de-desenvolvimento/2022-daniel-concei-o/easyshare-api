import { ObjectType } from '@nestjs/graphql'


import {
  FilterableField,
  FilterableRelation
} from '@nestjs-query/query-graphql'


import { BaseDTO } from 'src/modules/bases/dto/base.dto'
import { UserDTO } from 'src/modules/users/dto/user.dto'

@ObjectType('CreditCard')

// TODO: FIX nessa Relação
@FilterableRelation('user', () => UserDTO)
export class CreditCardDTO extends BaseDTO {
  @FilterableField({ nullable: true })
  zip_code: string

  @FilterableField({ nullable: true })
  city: string

  @FilterableField({ nullable: true })
  state: string

  @FilterableField({ nullable: true })
  country: string

  @FilterableField({ nullable: true })
  street: string

  @FilterableField({ nullable: true })
  complement: string

  @FilterableField({ nullable: true })
  holder_name: string

  @FilterableField({ nullable: true })
  holder_document: string

  @FilterableField({ nullable: true })
  brand: string

  @FilterableField({ nullable: true })
  exp_month: number

  @FilterableField({ nullable: true })
  exp_year: number

  @FilterableField({ nullable: true })
  status: string

  @FilterableField({ nullable: true })
  type: string

   @FilterableField({ nullable: true })
   first_six_digits: string

   @FilterableField({ nullable: true })
   last_four_digits: string
}