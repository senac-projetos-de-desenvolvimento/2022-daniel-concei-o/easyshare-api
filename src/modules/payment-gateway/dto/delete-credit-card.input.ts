import { Field, InputType } from '@nestjs/graphql'

@InputType('deleteCreditCard')
export class deleteCreditCardInput {
  @Field()
  cardId: string
}
