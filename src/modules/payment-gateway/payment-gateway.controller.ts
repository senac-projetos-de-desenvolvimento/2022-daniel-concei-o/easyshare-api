import { Body, Controller, Injectable, Post } from '@nestjs/common'
import { PaymentGatewayService } from './payment-gateway.service'
import { MonetaryTransactionsService } from '../monetary-transactions/monetary-transactions.service'
import { GatewayPagarmeWebhookInput } from './dto/create-payment-gateway.input'


@Injectable()
@Controller('pagarme')
export class GatewayPagarmeController {
  constructor(
    public paymentGatewayService: PaymentGatewayService,
    private monetaryTransactionsService: MonetaryTransactionsService
  ) {}

  @Post('webhooks')
  async orderStatusChanged(
    @Body() body: GatewayPagarmeWebhookInput
  ): Promise<void> {
    await this.monetaryTransactionsService.orderStatusChanged(body)
  }
}