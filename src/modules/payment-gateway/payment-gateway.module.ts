import { Module } from "@nestjs/common";
import { PaymentGatewayService } from "./payment-gateway.service";
import { PaymentGatewayResolver } from "./payment-gateway.resolver";
import { UsersModule } from "../users/users.module";
import { Adress } from "../adresses/entities/adress.entity";
import { NestjsQueryGraphQLModule } from "@nestjs-query/query-graphql";
import { NestjsQueryTypeOrmModule } from "@nestjs-query/query-typeorm";
import { GatewayPagarmeController } from "./payment-gateway.controller";
import { MonetaryTransactionsService } from "../monetary-transactions/monetary-transactions.service";
import { HttpModule } from "@nestjs/axios/dist";

@Module({
  controllers: [GatewayPagarmeController],
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryTypeOrmModule.forFeature([Adress]),
        UsersModule,
        HttpModule,
      ],
      resolvers: [],
    }),
  ],

  providers: [
    PaymentGatewayResolver,
    PaymentGatewayService,
    MonetaryTransactionsService,
  ],
  exports: [PaymentGatewayService, PaymentGatewayResolver],
})
export class PaymentGatewayModule {}
