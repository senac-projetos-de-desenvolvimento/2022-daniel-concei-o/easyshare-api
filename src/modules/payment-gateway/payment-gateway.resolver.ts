import { Resolver, Mutation, Args, Query } from "@nestjs/graphql";
import { PaymentGatewayService } from "./payment-gateway.service";
import { PaymentGateway } from "./entities/payment-gateway.entity";
import { UserDTO } from "../users/dto/user.dto";
import { CurrentUser } from "../decorator/current-user";
import { User } from "../users/entities/user.entity";
import { UseGuards } from "@nestjs/common";
import { GqlAuthGuard } from "../auth/auth.guard";
import { RecipientDTO } from "../donee/dto/recipient.dto";
import { CreateBankAccountInput } from "../users/dto/create-banck-account.inut";
import { CreateUserInput } from "../users/dto/create-user.input";

@Resolver(() => PaymentGateway)
export class PaymentGatewayResolver {
  constructor(private paymentGatewayService: PaymentGatewayService) {}

  @Mutation(() => UserDTO)
  @UseGuards(GqlAuthGuard)
  public async createOneCustomerGateway(
    @CurrentUser() user: User,
    @Args("data" ,{nullable: true}) data: CreateUserInput
  ): Promise<any> {
    const response = await this.paymentGatewayService.createGatewayToDonator(
      user,
      data
    );

    return response;
  }

  @Mutation(() => RecipientDTO)
  @UseGuards(GqlAuthGuard)
  public async createOneRecipientGateway(
    @CurrentUser() currentUser: User,
    @Args("data") data: CreateBankAccountInput
  ): Promise<RecipientDTO> {
    const response = await this.paymentGatewayService.createGatewayToRecipient(
      currentUser,
      data
    );
    return response;
  }


  @Query(() => RecipientDTO)
  @UseGuards(GqlAuthGuard)
  public async getCurrentRecipient(
    @CurrentUser() currentUser: User,
    @Args("recipientId") recipientId: string
  ): Promise<RecipientDTO> {
    const response = await this.paymentGatewayService.getOneRecipient(
      currentUser,
      recipientId
    );

    return response;
  }



  @Mutation(() => RecipientDTO)
  @UseGuards(GqlAuthGuard)
  public async updateCurrentRecipientAccount(
    @CurrentUser() currentUser: User,
    @Args("data") data: CreateBankAccountInput,
    @Args("recipientId") recipientId: string
  ): Promise<any> {
    const response = await this.paymentGatewayService.updateRecipienteAccount(
      currentUser,
      data,
      recipientId
    );
    return response;
  }
}

