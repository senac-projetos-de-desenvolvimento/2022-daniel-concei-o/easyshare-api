import { HttpException, Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { User } from "../users/entities/user.entity";
import * as consts from "../../common/constants/error.constants";
import { BasicAuth } from "src/common/utils/basic-auth.util";
import { CreateBankAccountInput } from "../users/dto/create-banck-account.inut";
import { RecipientDTO } from "../donee/dto/recipient.dto";
import { CreditCardDTO } from "./dto/credit-card.dto";
import { deleteCreditCardInput } from "./dto/delete-credit-card.input";
import axios from "axios";
import { CreateUserInput } from "../users/dto/create-user.input";

@Injectable()
export class PaymentGatewayService {
  constructor(private usersService: UsersService) {}

  async updateRecipienteAccount(
    user: User,
    data: CreateBankAccountInput,
    userRecipientId: string
  ): Promise<any> {
    const loggedUser = await this.getDataByUserToken(user);
    const header = this.setConnectionHeaders()

    const recipientId = userRecipientId || loggedUser.recipientId;

    let response = null;

    await axios
      .patch(
        `${process.env.PAYMENT_GATEWAY_URL}/recipients/${recipientId}/default-bank-account`,
        {
          bank_account: {
            holder_name: data.holder_name,
            holder_type: "individual",
            holder_document: data.CPF,
            bank: data.bank,
            account_check_digit: data.account_check_digit,
            account_number: data.account_number,
            branch_check_digit: data.branch_check_digit,
            branch_number: data.branch_number,
            type: "savings",
          },
        },
        header
      )
      .then((res) => {
        const resData: RecipientDTO = {
          name: res.data.name,
          email: res.data.email,
          recipientId: res.data.id,
          id: res.data.default_bank_account.id,
          holder_name: res.data.default_bank_account.holder_name,
          holder_type: res.data.default_bank_account.holder_type,
          holder_document: res.data.default_bank_account.holder_document,
          bank: res.data.default_bank_account.bank,
          branch_number: res.data.default_bank_account.branch_number,
          branch_check_digit: res.data.default_bank_account.branch_check_digit,
          account_number: res.data.default_bank_account.account_number,
          account_check_digit:
            res.data.default_bank_account.account_check_digit,
          type: res.data.default_bank_account.type,
          status: res.data.default_bank_account.status,
        };
        response = resData;
      })
      .catch((err) => {
        console.log(err);
        const errors = err?.response?.data?.errors;
        const error = Object.values(errors)?.[0]?.[0];
        throw new HttpException(
          "RECIPIENT_GATEWAY_ERROR_READING_RECIPIENT- " + error,
          err?.response?.status
        );
      });

    console.log(response);
    if (response) {
      return response;
    } else {
      console.log("ERRO DE RECIPIENT");
      throw new Error(consts.ERROR_RECIPIENT_ID);
    }
  }

  async getOneRecipient(user: User, userRecipientId: string): Promise<any> {
    const loggedUser = await this.getDataByUserToken(user);
    const header = this.setConnectionHeaders()

    const recipientId = userRecipientId || loggedUser.recipientId;

    let response = null;

    await axios
      .get(`${process.env.PAYMENT_GATEWAY_URL}/recipients/${recipientId}`, header)
      .then((res) => {
        const resData: RecipientDTO = {
          name: res.data.name,
          email: res.data.email,
          recipientId: res.data.id,
          id: res.data.default_bank_account.id,
          holder_name: res.data.default_bank_account.holder_name,
          holder_type: res.data.default_bank_account.holder_type,
          holder_document: res.data.default_bank_account.holder_document,
          bank: res.data.default_bank_account.bank,
          branch_number: res.data.default_bank_account.branch_number,
          branch_check_digit: res.data.default_bank_account.branch_check_digit,
          account_number: res.data.default_bank_account.account_number,
          account_check_digit:
            res.data.default_bank_account.account_check_digit,
          type: res.data.default_bank_account.type,
          status: res.data.default_bank_account.status,
        };
        response = resData;
      })
      .catch((err) => {
        console.log(err);
        const errors = err?.response?.data?.errors;
        const error = Object.values(errors)?.[0]?.[0];
        throw new HttpException(
          "RECIPIENT_GATEWAY_ERROR_READING_RECIPIENT- " + error,
          err?.response?.status
        );
      });

    console.log(response);
    if (response) {
      return response;
    } else {
      console.log("ERRO DE RECIPIENT");
      throw new Error(consts.ERROR_RECIPIENT_ID);
    }
  }

  async createGatewayToDonator(
    user: User,
    inputUserData: CreateUserInput
  ): Promise<string | null> {
    const loggedUser = await this.getDataByUserToken(user);

    let response = null;
    let userInfo = null;

    const isUserDataCompleted = user.address;

    if (isUserDataCompleted) {
      const usePhoneDDD = loggedUser.telephone.substring(0, 2);
      const userPhone = loggedUser.telephone.substring(2);
      userInfo = {
        ...loggedUser,
        usePhoneDDD,
        userPhone,
      };
    } else {
      const usePhoneDDD = inputUserData.telephone.substring(0, 2);
      const userPhone = inputUserData.telephone.substring(2);
      userInfo = {
        ...inputUserData,
        usePhoneDDD,
        userPhone,
      };
    }

    const header = this.setConnectionHeaders()

    await axios
      .post(
        `${process.env.PAYMENT_GATEWAY_URL}/customers`,
        {
          name: userInfo.name,
          email: userInfo.email,
          code: loggedUser.id,
          document: userInfo.CPF,
          type: "individual",
          document_type: "CPF",
          address: {
            state: userInfo.address.state,
            country: "BR",
            line_1: `${userInfo.address.number}, ${userInfo.address.street}`,
            line_2: loggedUser?.address?.complement,
            zip_code: userInfo.address.CEP,
            city: userInfo.address.city,
          },
          birthdate: loggedUser.birthday,
          phones: {
            home_phone: {
              country_code: "55",
              area_code: userInfo.usePhoneDDD,
              number: userInfo.userPhone,
            },
            mobile_phone: {
              country_code: "55",
              area_code: userInfo.usePhoneDDD,
              number: userInfo.userPhone,
            },
          },
        },
        header
      )
      .then((res) => {
        response = res.data;
      })
      .catch((err) => {
        console.log(err);
        const errors = err?.response?.data?.errors;
        const error = Object.values(errors)?.[0]?.[0];
        throw new HttpException(
          "RECIPIENT_GATEWAY_ERROR_WHEN_CREATING_CARD- " + error,
          err?.response?.status
        );
      });

    if (response) {
      try {
        loggedUser.donatorId = response.id;

        delete loggedUser.password;

        await this.usersService.repo.save(loggedUser);
      } catch (error) {
        console.log("ERRO DE SAVE REPO", error);
        throw new Error(consts.ERROR_RECIPIENT_ID);
      }
    }

    return response;
  }


  async createGatewayToRecipient(
    user: User,
    data: CreateBankAccountInput
  ): Promise<RecipientDTO | null> {
    const loggedUser = await this.getDataByUserToken(user);

    let response = null;
    const header = this.setConnectionHeaders()


    await axios
      .post(
        `${process.env.PAYMENT_GATEWAY_URL}/recipients`,
        {
          name: loggedUser?.name,
          email: loggedUser?.email,
          document: loggedUser?.CPF,
          type: 'individual',
          default_bank_account: {
            holder_name: data?.holder_name,
            holder_type: data?.type,
            holder_document: data?.CPF,
            bank: data?.bank,
            branch_number: data?.branch_number,
            branch_check_digit: data?.branch_check_digit,
            account_number: data?.account_number,
            account_check_digit: data?.account_check_digit,
            type: 'savings',
          },
          transfer_settings: {
            transfer_enabled: false,
            transfer_interval: 'Monthly',
            transfer_day: 1,
          },
          automatic_anticipation_settings: {
            enabled: false,
            type: 'full',
            volume_percentage: '50',
            delay: null,
          },
          // metadata: { key: 'value' },
        },
        header
      )
      .then(async (res) => {
        const resData: RecipientDTO = {
          name: res.data.name,
          email: res.data.email,
          recipientId: res.data.id,
          id: res.data.default_bank_account.id,
          holder_name: res.data.default_bank_account.holder_name,
          holder_type: res.data.default_bank_account.holder_type,
          holder_document: res.data.default_bank_account.holder_document,
          bank: res.data.default_bank_account.bank,
          branch_number: res.data.default_bank_account.branch_number,
          branch_check_digit: res.data.default_bank_account.branch_check_digit,
          account_number: res.data.default_bank_account.account_number,
          account_check_digit:
            res.data.default_bank_account.account_check_digit,
          type: res.data.default_bank_account.type,
          status: res.data.default_bank_account.status,
        };
        console.log("resData", resData);

        response = resData;
      })
      .catch((err) => {
        const erro = err?.response?.data?.message;
        console.log("ERRO: ", err)

        throw new HttpException(
          "RECIPIENT_GATEWAY_ERROR_WHEN_CREATING_RECIPIENT - " + erro,
          err?.response?.status
        );
      });

    if (!!response) {
      try {
        loggedUser.bankAccountId = response.id;
        loggedUser.recipientId = response.recipientId;

        delete loggedUser.password;

        await this.usersService.repo.save(loggedUser);
      } catch (error) {
        throw new Error(consts.ERROR_RECIPIENT_ID);
      }
    }
    return response;
  }

  async getBalanceBankAccount(user: User, data: string) {
    const loggedUser = await this.getDataByUserToken(user);

    let response = null;
    const header = this.setConnectionHeaders()

    await axios
      .get(
        `${process.env.PAYMENT_GATEWAY_URL}/recipients/${loggedUser.recipientId}/balance`,
        header
      )
      .then(async (res) => {
        response = res.data;
      })
      .catch(function (error) {
        console.error(error);
      });

    console.log(response);
    return response;
  }

  async listGatewayCardsByUser(user: User, data: string) {
    const loggedUser = await this.getDataByUserToken(user);
    let response = null;
    console.log(data);

    const header = this.setConnectionHeaders()

    await axios
      .get(
        `${process.env.PAYMENT_GATEWAY_URL}/customers/${loggedUser.donatorId}/cards`,
        header
      )
      .then(async (res) => {
        response = res.data.data;
      })
      .catch(function (error) {
        console.error(error);
      });

    console.log(response);

    const cards: CreditCardDTO[] = response;
    let i = 0;
    for (const res of response) {
      cards[i].brand = res.brand;
      cards[i].exp_month = res.exp_month;
      cards[i].exp_year = res.exp_year;
      cards[i].id = res.id;
      cards[i].first_six_digits = res.first_six_digits;
      cards[i].last_four_digits = res.last_four_digits;
      cards[i].holder_name = res.holder_name;
      cards[i].zip_code = res.billing_address.zip_code;
      cards[i].city = res.billing_address.city;
      cards[i].state = res.billing_address.state;
      cards[i].country = res.billing_address.country;
      cards[i].street = res.billing_address.line_1;
      cards[i].complement = res.billing_address.line_2;
      i++;
    }

    return cards;
  }

  async deleteGatewayCardByUser(user: User, data: deleteCreditCardInput) {
    const loggedUser = await this.getDataByUserToken(user);

    let response = null;

    const header = this.setConnectionHeaders()

    await axios
      .delete(
        `${process.env.PAYMENT_GATEWAY_URL}/customers/${loggedUser.donatorId}/cards/${data.cardId}`,
        header
      )
      .then(async (res) => {
        response = res.data;
      })
      .catch(function (error) {
        console.error(error);
      });

    if (response) {
      try {
        loggedUser.cardId = null;
        delete loggedUser.password;
        await this.usersService.repo.save(loggedUser);
      } catch (error) {
        throw new Error(consts.ERROR_CARD_ID);
      }

      return response;
    }
  }

  async getDataByUserToken(user: User) {
    const loggedUser = await this.usersService.getUserById(user.id);
    if (!loggedUser) {
      throw new Error(consts.USER_NOT_FOUND);
    }
    return loggedUser;
  }

  private setConnectionHeaders(){
    const gatewayToken = BasicAuth.getGatewayToken();

    return {
      headers: {
        Accept: "application/json",
        "content-type": "application/json",
        Authorization: `Basic ${gatewayToken}`,
      },
    }
  }
}
