import { UpdateUserInput } from '@easyshare/modules/users/dto/update-user.input';
import { InputType, Field } from '@nestjs/graphql';
import { IsString } from 'class-validator';
import { PixPaymentKeyTypeEnum } from '../enum/pixPaymentKeyEnum.type';

@InputType('CreatePixPaymentKeyInput')
export class CreatePixPaymentKeyInput {
  @IsString()
  @Field()
  pixKey?: string

  @IsString()
  @Field()
  type?: PixPaymentKeyTypeEnum

  @Field(() => UpdateUserInput)
  user?: UpdateUserInput
}
