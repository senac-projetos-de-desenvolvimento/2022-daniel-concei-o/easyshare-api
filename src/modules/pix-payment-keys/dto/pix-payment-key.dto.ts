import { PixPaymentKeyTypeEnum } from "../enum/pixPaymentKeyEnum.type";
import { ObjectType } from "@nestjs/graphql";
import { BaseDTO } from "@easyshare/modules/bases/dto/base.dto";
import { FilterableField } from "@nestjs-query/query-graphql";


@ObjectType('PixPaymentKey')
export class PixPaymentKeyDTO extends BaseDTO {
  @FilterableField()
  pixKey: string

  @FilterableField(() => PixPaymentKeyTypeEnum, { nullable: true })
  type: PixPaymentKeyTypeEnum
}
