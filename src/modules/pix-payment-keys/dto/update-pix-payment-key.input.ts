import { CreatePixPaymentKeyInput } from './create-pix-payment-key.input';
import { InputType, Field, Int, PartialType, ID } from '@nestjs/graphql';

@InputType()
export class UpdatePixPaymentKeyInput extends PartialType(CreatePixPaymentKeyInput) {
  @Field(() => ID)
  id?: string
}
