import { BaseEntity } from "@easyshare/modules/bases/entities/base.entity";
import { User } from "@easyshare/modules/users/entities/user.entity";
import { Column, Entity, ManyToOne } from "typeorm";
import { PixPaymentKeyTypeEnum } from "../enum/pixPaymentKeyEnum.type";


@Entity()
export class PixPaymentKey extends BaseEntity {
  @Column({ nullable: true })
  pixKey: string

  @Column({ type: 'enum', enum: PixPaymentKeyTypeEnum, nullable: true })
  type: PixPaymentKeyTypeEnum

  @ManyToOne(() => User, user => user.pixKeys)
  user: User
}
