import { registerEnumType } from '@nestjs/graphql'

export enum PixPaymentKeyTypeEnum {
  CPF = 'CPF',

  TELEFONE = 'TELEFONE',
  EMAIL = 'EMAIL',
  RAMDOMKEY = 'RAMDOMKEY'
}

registerEnumType(PixPaymentKeyTypeEnum, {
  name: 'PixPaymentKeyTypeEnum'
})