import { Module } from '@nestjs/common';
import { PixPaymentKeysService } from './pix-payment-keys.service';
import { PixPaymentKeysResolver } from './pix-payment-keys.resolver';
import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { PixPaymentKey } from './entities/pix-payment-key.entity';
import { PixPaymentKeyDTO } from './dto/pix-payment-key.dto';
import { CreatePixPaymentKeyInput } from './dto/create-pix-payment-key.input';
import { UpdatePixPaymentKeyInput } from './dto/update-pix-payment-key.input';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([PixPaymentKey])],
      resolvers: [
        {
          DTOClass: PixPaymentKeyDTO,
          EntityClass: PixPaymentKey,
          CreateDTOClass: CreatePixPaymentKeyInput,
          UpdateDTOClass: UpdatePixPaymentKeyInput,
          enableTotalCount: true,
          pagingStrategy: PagingStrategies.OFFSET
        }
      ]
    })
  ],
  providers: [PixPaymentKeysResolver, PixPaymentKeysService]
})
export class PixPaymentKeysModule {}
