import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { PixPaymentKeysService } from './pix-payment-keys.service';
import { PixPaymentKey } from './entities/pix-payment-key.entity';
import { CreatePixPaymentKeyInput } from './dto/create-pix-payment-key.input';
import { UpdatePixPaymentKeyInput } from './dto/update-pix-payment-key.input';

@Resolver(() => PixPaymentKey)
export class PixPaymentKeysResolver {
  constructor(private readonly pixPaymentKeysService: PixPaymentKeysService) {}

}
