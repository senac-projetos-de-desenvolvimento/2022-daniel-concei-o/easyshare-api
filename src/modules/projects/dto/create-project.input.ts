import { InputType, Field } from "@nestjs/graphql";
import { CategoryEnum } from "../entities/enum/category.enum";

@InputType()
export class CreateProjectInput {
  @Field()
  name?: string;

  @Field()
  category?: CategoryEnum;

  @Field()
  finalDate?: Date;

  @Field({ nullable: true })
  justification?: string;

  @Field({ nullable: true })
  coverImage?: string;

  @Field({ nullable: true })
  videoLink?: string;

  @Field({ nullable: true })
  projectGoal?: number;

  @Field({ nullable: true })
  totalRaised?: number;

  @Field({ nullable: true })
  totalDonors?: number;

  @Field({ nullable: true })
  configAlert?: boolean;

  userId: string;
}
