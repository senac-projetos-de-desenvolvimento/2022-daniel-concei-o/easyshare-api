import {
  FilterableField,
  FilterableOffsetConnection,
  FilterableRelation,
} from "@nestjs-query/query-graphql/dist/src/decorators";
import { Field, ObjectType, registerEnumType } from "@nestjs/graphql";
import { BaseDTO } from "src/modules/bases/dto/base.dto";
import { FileDTO } from "src/modules/files/dto/file.dto";
import { UserDTO } from "src/modules/users/dto/user.dto";
import { CategoryEnum } from "../entities/enum/category.enum";
import { FavoriteDTO } from "@easyshare/modules/favorites/dto/favorite.dto";

registerEnumType(CategoryEnum, {
  name: "CategoryEnum",
});

@ObjectType("Project")
@FilterableRelation("user", () => UserDTO, { nullable: true })
@FilterableOffsetConnection("images", () => FileDTO, { nullable: true })
@FilterableOffsetConnection("favorites", () => FavoriteDTO, {
  nullable: true,
  enableAggregate: true,
})
export class ProjectDTO extends BaseDTO {
  @FilterableField({ nullable: true })
  name?: string;

  @Field(() => CategoryEnum, { nullable: true })
  category?: CategoryEnum;

  @FilterableField({ nullable: true })
  finalDate?: Date;

  @Field({ nullable: true })
  justification?: string;

  @Field({ nullable: true })
  coverImage?: string;

  @Field({ nullable: true })
  videoLink?: string;

  @Field({ nullable: true })
  projectGoal?: number;

  @Field({ nullable: true })
  totalRaised?: number;

  @Field({ nullable: true })
  totalDonors?: number;

  @Field({ nullable: true })
  configAlert?: boolean;
}
