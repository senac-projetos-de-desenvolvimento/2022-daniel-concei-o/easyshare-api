import { registerEnumType } from "type-graphql";

export enum CategoryEnum {
  "ANIMAL" = "animals",
  "HABITATION" = "habitation",
  "EDUCATION" = "education",
  "EVENTS" = "events",
  "HUNGER" = "hunger",
  "SOCIAL" = "social",
  "HEALTH" = "health",
  "TRAGEDY" = "tragedy",
  "DREAM" = "dream",
  "OTHER" = "other",
}

registerEnumType(CategoryEnum, {
  name: "CategoryEnum",
});
