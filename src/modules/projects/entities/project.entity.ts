import { BaseEntity } from "src/modules/bases/entities/base.entity";
import { File } from "src/modules/files/entities/file.entity";
import { User } from "src/modules/users/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { CategoryEnum } from "./enum/category.enum";
import { Favorite } from "@easyshare/modules/favorites/entities/favorite.entity";

@Entity()
export class Project extends BaseEntity {
  @Column()
  name: string;

  @Column({
    nullable: true,
    name: "category",
    type: "enum",
    enum: CategoryEnum,
  })
  category: CategoryEnum;

  @Column()
  finalDate: Date;

  @Column()
  justification: string;

  @Column({ nullable: true })
  coverImage?: string;

  @Column({ nullable: true })
  videoLink: string;

  @Column({ nullable: true })
  projectGoal?: number;

  @Column({ nullable: true })
  totalRaised?: number;

  @Column({ nullable: true })
  totalDonors?: number;

  @Column()
  configAlert: boolean;

  @ManyToOne(() => User, (user) => user.projects, {
    nullable: true,
    eager: true,
  })
  @JoinColumn()
  user?: User;

  @Column()
  userId: string;

  @OneToMany(() => File, (images) => images.project, { eager: true })
  @JoinColumn()
  images: File[];

  @Column({ nullable: true })
  imagesId: string;

  @OneToMany(() => Favorite, (favorite) => favorite.project, {
    eager: true,
    nullable: true,
  })
  favorites: Favorite[];
}
