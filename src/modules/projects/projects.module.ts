import { Module } from "@nestjs/common";
import { ProjectsService } from "./projects.service";
import { ProjectsResolver } from "./projects.resolver";
import {
  NestjsQueryGraphQLModule,
  PagingStrategies,
} from "@nestjs-query/query-graphql";
import { NestjsQueryTypeOrmModule } from "@nestjs-query/query-typeorm";
import { Project } from "./entities/project.entity";
import { ProjectDTO } from "./dto/project.dto";
import { CreateProjectInput } from "./dto/create-project.input";
import { UpdateProjectInput } from "./dto/update-project.input";

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Project])],
      resolvers: [
        {
          DTOClass: ProjectDTO,
          EntityClass: Project,
          CreateDTOClass: CreateProjectInput,
          UpdateDTOClass: UpdateProjectInput,
          enableTotalCount: true,
          pagingStrategy: PagingStrategies.OFFSET,
        },
      ],
    }),
  ],
  // exports: [UsersService],
  providers: [ProjectsResolver, ProjectsService],
})
export class ProjectsModule {}
