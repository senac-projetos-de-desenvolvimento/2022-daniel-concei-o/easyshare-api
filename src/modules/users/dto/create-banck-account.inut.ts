import { Field, InputType } from '@nestjs/graphql'
import { BankAccountTypeEnum } from '../enum/bank-account-type.enum'

@InputType()
export class CreateBankAccountInput {
  //titular da conta
  @Field({ nullable: true })
  holder_name:string

  @Field({ nullable: true })
  bank: string

  @Field({ nullable: true })
  branch_number: string

  @Field({ nullable: true })
  branch_check_digit: string

  @Field({ nullable: true })
  account_number: string

  @Field({ nullable: true })
  account_check_digit: string

  @Field({ nullable: true })
  type?: BankAccountTypeEnum

  @Field()
  CPF?: string

  @Field({ nullable: true })
  cnpj?: string
}