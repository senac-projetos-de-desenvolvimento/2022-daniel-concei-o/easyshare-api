import { InputType, Field } from "@nestjs/graphql";
import { CreateAdressInput } from "src/modules/adresses/dto/create-adress.input";

@InputType()
export class CreateUserInput {
  @Field()
  name: string;

  @Field({ nullable: true })
  RG?: string;

  @Field()
  CPF: string;

  @Field({ nullable: true })
  birthday?: Date;

  @Field()
  email: string;

  @Field({ nullable: true })
  telephone?: string;

  @Field({ nullable: true })
  isActive?: boolean;

  // @Field({ nullable: true })
  // addressId?: string;
  @Field(() => CreateAdressInput, { nullable: true })
  address?: CreateAdressInput;

  @Field({ nullable: true })
  password?: string;
}
