import {
  FilterableField,
  FilterableOffsetConnection,
  FilterableRelation,
  OffsetConnection,
} from "@nestjs-query/query-graphql";
import { Field, HideField, ObjectType } from "@nestjs/graphql";

import { PixPaymentKeyDTO } from "@easyshare/modules/pix-payment-keys/dto/pix-payment-key.dto";
import { FavoriteDTO } from "@easyshare/modules/favorites/dto/favorite.dto";
import { BaseDTO } from "@easyshare/modules/bases/dto/base.dto";
import { AdressDTO } from "@easyshare/modules/adresses/dto/adress.dto";
import { ProjectDTO } from "@easyshare/modules/projects/dto/project.dto";

@ObjectType("User")
@FilterableRelation("address", () => AdressDTO, { nullable: true })
@OffsetConnection("projects", () => ProjectDTO, { nullable: true })
@FilterableOffsetConnection("pixKeys", () => PixPaymentKeyDTO, {
  nullable: true,
})
@FilterableOffsetConnection("favorites", () => FavoriteDTO, {
  nullable: true,
  enableAggregate: true,
})
export class UserDTO extends BaseDTO {
  @Field()
  id: string;

  @FilterableField()
  name: string;

  @FilterableField({ nullable: true })
  RG?: string;

  @FilterableField()
  CPF: string;

  @FilterableField()
  email: string;

  @FilterableField({ nullable: true })
  telephone?: string;

  @FilterableField({ nullable: true })
  birthday?: Date;

  @FilterableField({ nullable: true })
  isActive: boolean;

  @HideField()
  password?: string;

  @Field(() => AdressDTO, { nullable: true })
  address?: AdressDTO;

  @FilterableField({ nullable: true })
  recipientId?: string;

  @FilterableField({ nullable: true })
  donatorId?: string;

  @FilterableField({ nullable: true })
  bankAccountId?: string;

  @FilterableField({ nullable: true })
  cardId?: string;
}
