import { BaseEntity } from "../../bases/entities/base.entity";

import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Adress } from "src/modules/adresses/entities/adress.entity";
import { hashPasswordTransform } from "@easyshare/common/helpers/crypto";
import { Project } from "src/modules/projects/entities/project.entity";
import { MonetaryTransaction } from "@easyshare/modules/monetary-transactions/entities/monetary-transaction.entity";
import { PixPaymentKey } from "@easyshare/modules/pix-payment-keys/entities/pix-payment-key.entity";
import { Favorite } from "@easyshare/modules/favorites/entities/favorite.entity";

@Entity()
export class User extends BaseEntity {
  @Column()
  name: string;

  @Column({ nullable: true })
  RG?: string;

  @Column({ nullable: true })
  birthday?: Date;

  @Column()
  CPF: string;

  @Column({ nullable: true })
  cnpj?: string;

  @Column()
  email: string;

  @Column({ nullable: true })
  telephone?: string;

  @Column({ nullable: true })
  isActive: boolean;

  @ManyToOne(() => Adress, (address) => address.users, {
    nullable: true,
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  address: Adress;

  @Column({ nullable: true })
  addressId: string;

  @Column({
    transformer: hashPasswordTransform,
    nullable: true,
  })
  password?: string;

  @OneToMany(() => Project, (projects) => projects.user, { nullable: true })
  projects?: Project[];

  @Column({ nullable: true })
  donatorId: string

  @Column({ nullable: true })
  recipientId: string

  @Column({ nullable: true })
  bankAccountId: string

  @Column({ nullable: true })
  cardId: string


  @OneToMany(() => MonetaryTransaction, transaction => transaction.user, {
    nullable: true
  })
  transactions: MonetaryTransaction[]

  @OneToMany(() => PixPaymentKey, pixPaymentKey => pixPaymentKey.user, {
    nullable: true
  })
  @JoinColumn()
  pixKeys: PixPaymentKey[];

  @OneToMany(() => Favorite, (favorites) => favorites.user, { nullable: true })
  favorites: Favorite[];
}
