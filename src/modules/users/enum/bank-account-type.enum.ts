import { registerEnumType } from '@nestjs/graphql'

export enum BankAccountTypeEnum {
  CHECKING = 'checking',
  SAVINGS = 'savings'
}

registerEnumType(BankAccountTypeEnum, {
  name: 'BankAccountTypeEnum'
})
