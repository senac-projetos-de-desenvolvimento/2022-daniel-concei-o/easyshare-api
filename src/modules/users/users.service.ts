import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserDTO } from "./dto/user.dto";
import { User } from "./entities/user.entity";
import * as consts from "../../common/constants/error.constants";
import { TypeOrmQueryService } from "@nestjs-query/query-typeorm";
import { SuccessGenericDTO } from "@easyshare/common/dto/success-generic.dto";

@Injectable()
export class UsersService extends TypeOrmQueryService<User> {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {
    super(userRepository, { useSoftDelete: false });
  }

  async getUserByEmail(email: string): Promise<UserDTO> {
    const user = await this.userRepository.findOne({ where: { email } });
    if (!user) {
      throw new NotFoundException(consts.USER_NOT_FOUND);
    }
    return user;
  }

  async getUserById(id: string): Promise<UserDTO> {
    const user = await this.userRepository.findOne(id);
    if (!user) {
      throw new NotFoundException(consts.USER_NOT_FOUND);
    }
    return user;
  }

  async validateEmail(email: string): Promise<SuccessGenericDTO> {
    const user = await this.userRepository.findOne({ where: { email } });

    if (user) {
      return {
        success: false,
        message: "Email already existes",
      };
    }

    return {
      success: true,
      message: "Email clear for use",
    };
  }

  async validateCpf(cpf: string): Promise<SuccessGenericDTO> {
    const user = await this.userRepository.findOne({ where: { CPF: cpf } });

    if (user) {
      return {
        success: false,
        message: "CPF already existes",
      };
    }
    return {
      success: true,
      message: "CPF clear for use",
    };
  }
}
